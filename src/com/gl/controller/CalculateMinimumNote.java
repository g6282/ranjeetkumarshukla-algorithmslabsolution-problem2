package com.gl.controller;

import java.util.Arrays;
import java.util.Scanner;

import com.gl.services.CalculateNotes;

public class CalculateMinimumNote {

	public static void main(String[] args) {
		int currencySizeArr = 0;
		int[] currencyArr;
		int amount = 0;

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the size of currency denominations");
		currencySizeArr = scanner.nextInt();

		// initialize array with size
		currencyArr = new int[currencySizeArr];

		// Taking the value from user in array
		System.out.println("Enter the currency denominations value");
		for (int i = 0; i < currencySizeArr; i++) {
			currencyArr[i] = scanner.nextInt();
		}

		System.out.println("Enter the amount you want to pay");
		amount = scanner.nextInt();

		System.out.println("Your payment approach in order to give min no of notes will be");
		Arrays.sort(currencyArr);
		CalculateNotes calculateNotes = new CalculateNotes();
		int[] notesArr = calculateNotes.calculateMinimumNotes(currencyArr, amount);
		for (int i = 0; i < notesArr.length; i++) {
			if (notesArr[i] != 0) {
				System.out.println(currencyArr[i] + " : " + notesArr[i]);
			}
		}
		scanner.close();
	}
}
