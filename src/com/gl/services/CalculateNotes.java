package com.gl.services;

public class CalculateNotes {
	// This function calculate the minimum notes for given amount
	public int[] calculateMinimumNotes(int[] currencyArr, int amount) {
		int[] noteCounter = new int[currencyArr.length];
		for (int i = currencyArr.length - 1; i >= 0; i--) {
			if (amount >= currencyArr[i]) {
				noteCounter[i] = amount / currencyArr[i];
				amount = amount - noteCounter[i] * currencyArr[i];
			}
		}
		return noteCounter;
	}
}
